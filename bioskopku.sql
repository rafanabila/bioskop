-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2018 at 09:05 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bioskopku`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id_film` int(11) NOT NULL,
  `judul_film` varchar(50) DEFAULT NULL,
  `gambar_film` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id_film`, `judul_film`, `gambar_film`, `deskripsi`) VALUES
(1, 'Hujan Bulan Juni', 'hujansah.jpg', 'Pingkan (Velove Vexia), dosen muda Sastra Jepang Universitas Indonesia, mendapat kesempatan belajar ke Jepang selama 2 tahun. Sarwono (Adipati Dolken) nelangsa mendengar kabar ditinggal Pingkan, yang selama ini hampir tidak pernah lepas dari sampingnya. \r\n\r\nSarwono ditugaskan Kaprodinya untuk presentasi kerjasama ke Universitas Sam Ratulangi Manado. Sarwono pun membawa Pingkan sebagai guide-nya selama di Manado. Pingkan bertemu keluarga besar almarhum ayahnya yang Manado. Ia mulai dipojokkan oleh pertanyaan tentang hubungannya dengan Sarwono. Apalagi kalau bukan masalah perbedaan yang di mata mereka sangat besar. Bukannya Pingkan (dan Sarwono) tidak menyadarinya. Mereka sudah terlanjur nyaman menetap bertahun-tahun di dalam ruangan kedap suara bernama kasih sayang...'),
(2, 'Thor Ragnarok', 'thor.jpg', 'Setelah mendengar muncul berbagai permasalahan di tempat asalnya, Thor (Chris Hemsworth) pulang ke Asgard. Ia kehilangan senjata andalannya dan terperangkap dalam sebuah penjara. \r\n\r\nNamun saat itu juga ia harus berlomba dengan waktu untuk menghentikan ‘Ragnarok’ dengan melawan Hela, musuh kuat yang ingin menghancurkan Asgard.'),
(3, 'Posesif', 'posesif.jpg', 'Hidup Lala jungkir balik. Bukan karena loncatan indahnya dari menara sepuluh meter, bukan pula karena ayahnya yang melatihnya dengan keras, tapi karena cinta pertamanya. Yudhis, murid baru di sekolahnya, berhasil menjebak hati Lala hanya untuknya.\r\n\r\nDi tahun terakhir SMA-nya, Lala ditarik keluar dari rutinitas lamanya. Hidupnya tak lagi melulu melihat birunya air kolam renang atau kusamnya dinding sekolah. Lala percaya cinta telah membebaskannya, sebab Yudhis selalu sigap menghadirkan pelangi asal Lala berjanji selamanya bersama.\r\n\r\nNamun perlahan Lala dan Yudhis harus menghadapi bahwa kasih mereka bisa hadirkan kegelapan. Cinta Yudhis yang awalnya tampak sederhana dan melindungi ternyata rumit dan berbahaya. Janji mereka untuk setia selamanya malah jadi jebakan.\r\n\r\nLala kini mengambang dalam pertanyaan: apa artinya cinta? Apakah seperti loncat indah, yang bila gagal, harus ia terus coba lagi atas nama kesetiaan? Ataukah ia hanya sedang tenggelam dalam kesia-siaan?'),
(4, 'Negeri Dongeng', 'negeri.jpg', 'Sebuah film mengenai perjalanan melihat Indonesia, melihat sahabat dan rekan-rekan seperjalanan, juga melihat diri sendiri. NEGERI DONGENG berkisah mengenai 7 sineas muda Indonesia yang mendaki 7 puncak gunung tertinggi di Nusantara berbekal 7 buah kamera, bersama-sama. "Dan mencintai tanah air Indonesia dapat ditumbuhkan dengan mengenal Indonesia bersama rakyatnya dari dekat. Pertumbuhan jiwa yang sehat dari pemuda harus berarti pula pertumbuhan fisik yang sehat. Karena itulah, kami naik gunung." -Soe Hok Gie- \r\n\r\nPerjalanan panjang membuat mereka mengupas cerita pada setiap tempat yang disinggahi. Beragam emosi berkecamuk dalam perjalanan. Pertemuan dengan orang-orang baru selama penjelajahan darat, laut, dan udara. Setiap potongan kisahnya akan memperlihatkan betapa Indonesia begitu kaya dan luas untuk dijelajahi bersama-sama. Dan di ujung perjalanan itu, kita akan menemukan arti sebuah perjalanan.'),
(5, 'One Fine Day', 'one.jpg', 'Pertemuan tidak terduga antara MAHESA ( Jefri Nichol ) dan ALANA ( Michelle Ziudith ) di Barcelona membuat keduanya menjadi akrab. Namun keakraban ini disalah gunakan oleh Mahesa. Ternyata cara pandang Alana dan Mahesa jauh berbeda. Akankah pertemuan itu menjadi sebuah cerita cinta antara ALANA dan MAHESA..?\r\n\r\nDANU ( Maxime Bouttier ) adalah kekasih ALANA di Barcelona. Danu selalu memberikan perhatian yg sangat istimewa kepada Alana, melalui barang-barang mewah. Danu berfikir itu cukup untuk ALANA. Suatu ketika Danu bertemu dengan Mahesa juga Alana. Danu mengingatkan Alana, untuk berhati-hati dengan MAHESA karena dia seorang penipu. Apakah ucapan DANU terbukti benar..?'),
(6, 'Mau Jadi Apa?', 'mau.jpg', '1997. SOLEH SOLIHUN berhasil masuk Universitas Padjadjaran. Namun kondisi kampus yang gersang dan lokasinya yang jauh dari mana-mana, membuat Soleh jadi meragukan pilihannya. "Ngapain sih gue di sini? Mau jadi apa nantinya?" bergolak dalam hati Soleh. Bersama teman-temannya yang sama galaunya—LUKMAN (BORIS BOKIR) si jago gambar; MARSYEL (ADJISDOAIBU), si hopeless romantic yang selalu sial; EKO (AWWE), si penggila musik yang doyan nyela; FEY (ANGGIKA BOLSTERLI), si blasteran yang cantik namun sedikit gila; serta si mulut besar SYARIF (RICKY WATTIMENA)—Soleh akhirnya mendirikan sebuah majalah kampus alternatif. Mengangkat hal-hal ringan seperti musik, film, olahraga dan gosip percintaan antar mahasiswa, majalah itu mereka beri nama Karung Goni, kependekan dari ''Kabar, Ungkapan, Gosip dan Opini''. \r\n\r\nMengelola Karung Goni tidaklah mudah. Soleh dan gengnya harus menghadapi cibiran dari anak-anak Fakta Jatinangor, majalah super serius yang lebih dulu mendominasi kampus, yang dipimpin oleh si arogan PANJI (RONAL SURAPRADJA). Bersamaan dengan itu, Soleh juga harus berjuang merebut hati ROS (AURELIE MOEREMANS), Slanker cantik yang telah membuatnya mabuk kepayang sejak ospek. Deretan karakter dan jalinan konflik ini hadir membentuk MAU JADI APA?—sebuah komedi inspiratif yang diangkat dari kisah nyata komika dan mantan jurnalis Soleh Solihun. Inilah bukti bahwa setiap orang punya jalannya sendiri dalam menjawab pertanyaan "mau jadi apa?".'),
(7, 'Kaili', 'kaili.jpg', 'Kisah ini berawal dari pertemuan pria dan wanita yang tak disengaja di sebuah perguruan tinggi ternama yang ada di kota Palu. Pertemuan sepasang insan manusia yang bernama Fajar dan Senja yang pada akhirnya menumbuhkan benih-benih cinta\r\n\r\nPada pandangan pertama kala itu, mereka tak langsung mengakui perasaan mereka masing- masing dan hanya berujung pada sebuah komunikasi melalui di Handphone. Setelah sama- sama menyelesaikan studi, kedunya memilih berkarir, Senja menjadi tenaga pengajar di sekolah swasta ternama sedangkan Fajar menghabiskan hari-harinya dengan bekerja di bengkel milik ayahnya.\r\n\r\nPerjalanan cinta Fajar dan Senja bak bunga-bunga di musim semi yang terus tumbuh dan berkembang. Hingga akhirnya Fajar memberanikan diri mengungkapkan cintanya dan juga disambut hangat oleh Senja. Namun perjalanan cinta mereka sebenarnya terasa kian berat ketika Fajar yang hanya berstatus sosial biasa-biasa saja sedangkan senja adalah keluarga yang berada.\r\n\r\nKeyakinan Fajar akan cintanya membuatnya memberanikan diri menemui ayah Senja, namun siapa sangka sang ayah merupakan penghalang dari dua insan yang dilanda asmara tersebut dan adat istiadat kesukuan pun ikut terbawa-bawa sehingga menjadi bumerang bagi hubungan mereka. Fajar merupakan seorang pria suku Kaili harus dilepaskan oleh Senja sebab paham yang berkembang dimasyarakat tentang suku Kaili memiliki sikap buruk dan tidak pantas untuk menjadi pendamping hidup.\r\n\r\nUjian demi ujian silih berganti menghampiri kehidupan Fajar membuatnya harus meninggalkan cintanya dan menjemput harga dirinya sebagai lelaki yang tak ingin dipandang sebelah mata hanya atas dasar kesukuan. Fajar meneguhkan niatnya merantau ke kota lain. Berbagai tantangan hidup telah dilalui bertahun-tahun dalam perantauan hingga akhirnya Fajar menduduki jabatan penting dalam sebuah perusahaan media tempatnya bekerja.\r\n\r\nAkankah kisah Fajar dan Senja berakhir bahagia atau terhenti akibat sudut pandang adat istiadat kesukuan yang masih mendarah daging dan tak pernah terlepas dari paradigma masyarakat moderen sekalipun?'),
(8, 'Susah Sinyal', 'susah.jpg', 'Ellen (Adinia Wirasti), pengacara yang sukses, adalah seorang single mom yang jarang bisa meluangkan waktu bagi anak tunggalnya Kiara (Aurora Ribero), yang akhirnya tumbuh sebagai remaja pemberontak yang lebih banyak melampiaskan emosinya di media sosial. Mereka tinggal bersama Agatha (Niniek L. Karim), ibunda Ellen yang sangat menyayangi Kiara. Ketika Agatha meninggal terkena serangan jantung, Kiara yang sejak kecil sangat dekat dengan Omanya langsung terguncang. Oleh psikolog, Ellen disarankan untuk mengajak Kiara berlibur, menghabiskan quality time untuk mengobati masa-masa di mana Ellen terlalu sibuk bekerja. Mereka pun pergi ke Sumba, menghabiskan saat-saat menyenangkan berdua. Kiara pulang dengan hati riang. \r\n\r\nDi Jakarta, Ellen langsung disambut masalah besar di kantor. Proyek besar yang sedang ia tangani bersama Iwan (Ernest Prakasa) terancam berantakan. Akhirnya karna sibuk, Ellen tidak menepati janjinya untuk menonton Kiara tampil di perlombaan talent show antar SMA yang sudah Kiara persiapkan sejak lama. Kiara pun marah dan pergi ke Sumba sendirian, tempat dimana terakhir kali ia bisa merasakan secercah kebahagiaan. Akankah Kiara bisa percaya lagi pada ibunya? Apakah Ellen sanggup merajut kembali keluarga kecil ini? Saksikan SUSAH SINYAL, tayang di bioskop mulai 21 Desember.');

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE `jam` (
  `id_jam` int(11) NOT NULL,
  `jam` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`id_jam`, `jam`) VALUES
(1, '12:30:00'),
(2, '14:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Romance'),
(2, 'Action'),
(3, 'Romance'),
(4, 'Drama'),
(5, 'Romance'),
(6, 'Drama'),
(7, 'Drama'),
(8, 'Drama');

-- --------------------------------------------------------

--
-- Table structure for table `kursi`
--

CREATE TABLE `kursi` (
  `id_kursi` int(11) NOT NULL,
  `id_studio` int(11) DEFAULT NULL,
  `no_kursi` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kursi`
--

INSERT INTO `kursi` (`id_kursi`, `id_studio`, `no_kursi`) VALUES
(1, 1, 'A1'),
(2, 1, 'A2'),
(3, 2, 'B1'),
(4, 2, 'B2');

-- --------------------------------------------------------

--
-- Table structure for table `nota`
--

CREATE TABLE `nota` (
  `id_nota` int(11) NOT NULL,
  `id_pembeli` int(11) NOT NULL,
  `tgl_pembelian` date NOT NULL,
  `grandtotal` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `bukti` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota`
--

INSERT INTO `nota` (`id_nota`, `id_pembeli`, `tgl_pembelian`, `grandtotal`, `status`, `bukti`) VALUES
(5, 9, '2018-04-06', 25000, 'LUNAS', '2.JPG'),
(8, 8, '2018-04-06', 100000, 'LUNAS', '31.JPG'),
(9, 9, '2018-04-06', 50000, '', '21.JPG'),
(10, 9, '2018-04-06', 25000, '', ''),
(11, 8, '2018-04-06', 50000, '', ''),
(12, 8, '2018-04-06', 50000, '', ''),
(14, 8, '2018-04-06', 50000, '', '12.JPG'),
(15, 8, '2018-04-06', 50000, '', ''),
(20, 13, '2018-05-03', 0, '', ''),
(21, 13, '2018-05-03', 25000, '', ''),
(22, 13, '2018-05-03', 75000, '', ''),
(23, 9, '2018-05-17', 75000, '', 'Test_Bing_Adista_02_XI_R_3.png');

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id_pembeli` int(11) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id_pembeli`, `nama`, `email`, `username`, `password`) VALUES
(1, 'Adis', 'ads@hiha', 'aq', 'aq'),
(2, 'Salma', '', '', ''),
(3, 'Mufa', '', '', ''),
(7, 'Adista Farras', 'adisaltianira@gmail.com', 'aku', 'aku'),
(8, 'Adista', 'adisaltianira@yahoo.com', 'adista', 'adista'),
(9, 'aa', 'a@a.com', 'aa', 'aa'),
(10, 'Adiss', 'adis@gmail.com', 'adis', 'adis'),
(11, 'denis', 'denis@gmail.com', 'denis', 'denis'),
(12, 'salma elek', 'salmaelek@gmail.com', 'salma', 'salma'),
(13, '1', '1@gamil.com', '1', '123');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `id_nota` int(11) NOT NULL,
  `id_film` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_nota`, `id_film`, `jumlah`) VALUES
(1, 5, 2, 1),
(4, 8, 4, 1),
(5, 9, 3, 1),
(6, 10, 2, 1),
(7, 11, 4, 1),
(8, 12, 3, 1),
(10, 14, 2, 2),
(11, 15, 1, 2),
(14, 21, 1, 1),
(15, 22, 3, 3),
(16, 23, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama_petugas`, `status`) VALUES
(1, 'Fara', 'Admin'),
(2, 'Yoga', 'Teller'),
(3, 'Mala', 'Teller');

-- --------------------------------------------------------

--
-- Table structure for table `studio`
--

CREATE TABLE `studio` (
  `id_studio` int(11) NOT NULL,
  `studio` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studio`
--

INSERT INTO `studio` (`id_studio`, `studio`) VALUES
(1, 'S1'),
(2, 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `tayang`
--

CREATE TABLE `tayang` (
  `id_tayang` int(11) NOT NULL,
  `id_film` int(11) DEFAULT NULL,
  `id_jam` int(11) DEFAULT NULL,
  `id_studio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tayang`
--

INSERT INTO `tayang` (`id_tayang`, `id_film`, `id_jam`, `id_studio`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(3, 1, 1, 2),
(4, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `id_tiket` int(11) NOT NULL,
  `id_pembeli` int(11) DEFAULT NULL,
  `id_tayang` int(11) DEFAULT NULL,
  `id_kursi` int(11) DEFAULT NULL,
  `id_petugas` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`id_tiket`, `id_pembeli`, `id_tayang`, `id_kursi`, `id_petugas`, `tanggal`) VALUES
(1, 1, 1, 1, 2, '2017-09-29'),
(2, 2, 2, 2, 3, '2017-09-30'),
(5, 3, 2, 3, 2, '2017-09-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id_film`);

--
-- Indexes for table `jam`
--
ALTER TABLE `jam`
  ADD PRIMARY KEY (`id_jam`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kursi`
--
ALTER TABLE `kursi`
  ADD PRIMARY KEY (`id_kursi`),
  ADD KEY `fk_studio` (`id_studio`);

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`id_nota`),
  ADD KEY `id_pembeli` (`id_pembeli`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id_pembeli`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`),
  ADD KEY `id_nota` (`id_nota`),
  ADD KEY `id_film` (`id_film`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `studio`
--
ALTER TABLE `studio`
  ADD PRIMARY KEY (`id_studio`);

--
-- Indexes for table `tayang`
--
ALTER TABLE `tayang`
  ADD PRIMARY KEY (`id_tayang`),
  ADD KEY `fk_film` (`id_film`),
  ADD KEY `fk_jam` (`id_jam`),
  ADD KEY `fk_studio2` (`id_studio`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`id_tiket`),
  ADD KEY `fk_pembeli` (`id_pembeli`),
  ADD KEY `fk_tayang` (`id_tayang`),
  ADD KEY `fk_kursi` (`id_kursi`),
  ADD KEY `fk_petugas` (`id_petugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id_film` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `jam`
--
ALTER TABLE `jam`
  MODIFY `id_jam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kursi`
--
ALTER TABLE `kursi`
  MODIFY `id_kursi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nota`
--
ALTER TABLE `nota`
  MODIFY `id_nota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id_pembeli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `studio`
--
ALTER TABLE `studio`
  MODIFY `id_studio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tayang`
--
ALTER TABLE `tayang`
  MODIFY `id_tayang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
  MODIFY `id_tiket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `kursi`
--
ALTER TABLE `kursi`
  ADD CONSTRAINT `fk_studio` FOREIGN KEY (`id_studio`) REFERENCES `studio` (`id_studio`);

--
-- Constraints for table `tayang`
--
ALTER TABLE `tayang`
  ADD CONSTRAINT `fk_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id_film`),
  ADD CONSTRAINT `fk_jam` FOREIGN KEY (`id_jam`) REFERENCES `jam` (`id_jam`),
  ADD CONSTRAINT `fk_studio2` FOREIGN KEY (`id_studio`) REFERENCES `studio` (`id_studio`);

--
-- Constraints for table `tiket`
--
ALTER TABLE `tiket`
  ADD CONSTRAINT `fk_kursi` FOREIGN KEY (`id_kursi`) REFERENCES `kursi` (`id_kursi`),
  ADD CONSTRAINT `fk_pembeli` FOREIGN KEY (`id_pembeli`) REFERENCES `pembeli` (`id_pembeli`),
  ADD CONSTRAINT `fk_petugas` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`),
  ADD CONSTRAINT `fk_tayang` FOREIGN KEY (`id_tayang`) REFERENCES `tayang` (`id_tayang`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
