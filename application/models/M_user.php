<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function masuk(){
		$nama=$this->input->post('nama');
		$email=$this->input->post('email');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$data_simpan=array(
			'nama'=>$nama,
			'email'=>$email,
			'username'=>$username,
			'password'=>$password
		);
		$this->db->insert('pembeli', $data_simpan);
		if($this->db->affected_rows()>0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_login()
	{
		return $this->db->where('username', $this->input->post('username'))
						->where('password', $this->input->post('password'))
						->get('pembeli');
	}
	

}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */