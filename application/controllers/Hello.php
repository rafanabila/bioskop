<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hello extends CI_Controller {

	public function index()
	{
		$data['judul']="homepage";
		$data['nama']="Adista Farras Altianira";
		$this->load->view('hello_world', $data);
	}
	public function profil()
	{
		$data['judul']="Profil";
		$this->load->view('v_profil', $data);
	}

}

/* End of file Hello.php */
/* Location: ./application/controllers/Hello.php */