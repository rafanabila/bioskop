<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bioskop extends CI_Controller {

	public function index()
	{
		$data['konten']="home";
		$this->load->view('bioskop',$data);
	}
	public function tiket()
	{
		$data['konten']="tiket";
		$this->load->view('bioskop', $data);
	}
	public function coming()
	{
		$data['konten']="coming";
		$this->load->view('bioskop', $data);
	}
	public function ks()
	{
		$data['konten']="ks";
		$this->load->view('bioskop', $data);
	}
	public function tm_film()
	{
		$this->load->model('m_film');
		$data['tampil_film']=$this->m_film->tampil_film();
		$data['konten']="film";
		$data['judul']="Daftar film";
		$this->load->view('bioskop', $data);
	}	
	public function detail_film($id_film='')
	{
		$this->load->model('m_film');
		$data['detail']=$this->m_film->detail($id_film);
		$data['konten']="detail_film";
		$data['judul']="Detail film";
		if($this->session->userdata('login')==TRUE){
			$data['pesan']="PESAN";
			$data['url']=$data['url']="index.php/cart/add_cart/$id_film";;
		}else {
			$data['pesan']="LOGIN DULU SEBELUM PESAN";
			$data['url']="index.php/bioskop/login";
		}
		$this->load->view('bioskop', $data);
	}
	public function login()
	{
		if ($this->session->userdata('login')==TRUE) {
			redirect('bioskop','refresh');
		}else {
			$this->load->view('login');
		}

	}
	public function register()
	{
		$this->load->view('register');
		
	}
	public function simpan()
	{
		if($this->input->post('submit')){
			$this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run()==TRUE) {
				$this->load->model('m_user');
				$proses=$this->m_user->masuk();
		if($proses){
			$this->session->set_flashdata('pesan', 'sukses simpan');
			redirect('bioskop/login','refresh');
		} else {
			$this->session->set_flashdata('pesan', 'gagal simpan');
			$this->load->view('register', 'refresh');
			}
		}
		else {
			$this->session->set_flashdata('pesan', validation_errors());
			$this->load->view('register', 'refresh');
			}
		
		}
	}
	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run()==TRUE) {
				$this->load->model('m_user');
				if ($this->m_user->get_login()->num_rows()>0) {
					$data=$this->m_user->get_login()->row();
					$array = array(
						'login' => TRUE,
						'username' => $data->username,
						'password' => $data->password,
						'nama'=> $data->nama,
						'id_pembeli' => $data->id_pembeli
					);
					
					$this->session->set_userdata( $array );
					redirect('bioskop','refresh');
				}else {
					redirect('bioskop/login','refresh');
				}
			}
			else {
				$this->session->set_flashdata('pesan', validation_errors());
				redirect('bioskop/login','refresh');
			}
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('bioskop/login','refresh');
	}
}

/* End of file Bioskop.php */
/* Location: ./application/controllers/Bioskop.php */