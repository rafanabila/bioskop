<!DOCTYPE html>
<html>
<head>
  <title>Cinema 21</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/style.css">
  <link rel="icon" href="img/aaa.png">
  <script type="text/javascript" src="<?=base_url()?>asset/js/jquery-3.2.js"></script>
  <script type="text/javascript" src="<?=base_url()?>asset/js/bootstrap.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    <a class="navbar-brand" href="#"><img src="<?=base_url()?>asset/img/logo.png"></a>
   </div>
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> HOME</a></li>
        <li><a href="coming.php"><span class="glyphicon glyphicon-film"></span> COMING SOON</a></li>
        <li><a href="login.php"><span class="glyphicon glyphicon-log-out"></span> LOGOUT</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-registration-mark"></span> DAFTAR</a></li>
    </ul>
    </div>
  </div>
</nav>