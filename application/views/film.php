<br><br><br>
<div class="col-md-12 jarak-atas">
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 align="center" style="font-size: 25pt; font-family: Century Gothic; font-style: bold;">Daftar Film Bioskop</h2>
	</div>
<div class="panel-body" style="background-color: white">
<br><br>
<?php 
	foreach ($tampil_film as $film) {
?>
<div class="col-md-3">
	<a href="<?=base_url('index.php/bioskop/detail_film/'.$film->id_film)?>" class="thumbnail">
		<img src="<?= base_url()?>asset/img/<?= $film->gambar_film;?>" style="width: 100%">
		<div class="caption" align="center">
			<?= $film->judul_film;?>
		</div>
	</a>
</div>
<?php
	}
 ?>
</div></div></div>
