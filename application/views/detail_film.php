<div class="container jarak-atas">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
      <div class="panel-body">
<div class="col-md-5">
	<img style="width: 100%" src="<?=base_url()?>asset/img/<?= $detail->gambar_film;?>">
</div>
<div class="col-md-7">
	<table class="table table-hover table-striped" style="align-items: center;">
		<tr>
			<td>Judul Film</td>
			<td><?= $detail->judul_film;?></td>
		</tr>
		<tr>
			<td>Deskripsi</td>
			<td><?=$detail->deskripsi;?></td>
			<td></td>
		</tr>

	</table>
	<a href="<?=base_url($url)?>" class="btn btn-success"><?= $pesan ?></a>
</div>
</div>
</div>
</div>
</div>
</div>
