<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="<?=base_url()?>asset/img/padinton.jpg" alt="...">
      <div class="carousel-caption"><h2>Paddington 2</h2>
       
      </div>
    </div>
    <div class="item">
      <img src="<?=base_url()?>asset/img/justice.jpg" alt="...">
      <div class="carousel-caption"><h2>Justice League</h2>
      
      </div>
    </div>
    <div class="item">
      <img src="<?=base_url()?>asset/img/coco.jpg" alt="...">
      <div class="carousel-caption"><h2>Coco</h2>
       
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>

<div class="container jarak-atas">
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">Now Playing</h3>
      </div>
      <div class="panel-body">
          <div class="row">
            <div class="col-md-3">
            <a href="#gambar1" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/hujansah.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Hujan Bulan Juni</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-3">
            <a href="#gambar2" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/thor.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Thor Ragnarok</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-3">
            <a href="#gambar3" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/posesif.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Posesif</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-3">
            <a href="#gambar5" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/negeri.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Negeri Dongeng</h5>
                </div>
             </div>
             </a>
            </div>
             <div class="col-md-3">
            <a href="#gambar4" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/one.jpg" alt="...">
                <div class="caption">
                <h5 align="center">One Fine Day</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-3">
            <a href="#gambar6" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/mau.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Mau Jadi Apa?</h5>
                </div>
             </div>
             </a>
            </div>
             <div class="col-md-3">
            <a href="#gambar7" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/kaili.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Kaili</h5>
                </div>
             </div>
             </a>
            </div>
             <div class="col-md-3">
            <a href="#gambar8" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/susah.jpg" alt="...">
                <div class="caption">
                <h5 align="center">Susah Sinyal</h5>
                </div>
             </div>
             </a>
            </div>
        </div>
      </div>  
    </div>
    </div>
     <div class="col-md-4">
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading" align="center"><span class="glyphicon glyphicon-film"></span> JADWAL TAYANG</div>
  <!-- Table -->
  <table class="table">
    <tr>
      <td>FILM</td>
      <td>JAM TAYANG</td>
    </tr>
    <tr>
      <td>Hujan Bulan Juni</td>
      <td><a href="#">[12.00]</a> 
          <a href="#">[13.45]</a></td>
    </tr>
    <tr>
      <td>Thor Ragnarok</td>
      <td><a href="#">[12.15]</a> 
          <a href="#">[14.00]</a></td>
    </tr>
    <tr>
      <td>Posesif</td>
      <td><a href="#">[12.30]</a> 
          <a href="#">[14.15]</a></td>
    </tr>
     <tr>
      <td>Negeri Dongeng</td>
      <td><a href="#">[12.45]</a> 
          <a href="#">[14.30]</a></td>
    </tr>
     <tr>
      <td>One Fine Day</td>
      <td><a href="#">[15.15]</a>
          <a href="#">[17.00]</a> </td>
    </tr>
    <tr>
      <td>Mau Jadi Apa?</td>
      <td><a href="#">[15.30]</a>
          <a href="#">[17.15]</a> </td>
    </tr>
    <tr>
      <td>Kaili</td>
      <td><a href="#">[15.45]</a>
          <a href="#">[17.30]</a> </td>
    </tr>
    <tr>
      <td>Susah Sinyal</td>
      <td><a href="#">[16.00]</a>
          <a href="#">[17.45]</a> </td>
    </tr>
  </table>
</div>
</div>
</div>
</div>
</div>
</div>

<div id="gambar1" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Hujan Bulan Juni</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/7tgG8A_lejM">
      </iframe>
      <br>
        <p>Pingkan (Velove Vexia), dosen muda Sastra Jepang Universitas Indonesia, mendapat kesempatan belajar ke Jepang selama 2 tahun. Sarwono (Adipati Dolken) nelangsa mendengar kabar ditinggal Pingkan, yang selama ini hampir tidak pernah lepas dari sampingnya. </p>
        <br>
        <p>
        Sarwono ditugaskan Kaprodinya untuk presentasi kerjasama ke Universitas Sam Ratulangi Manado. Sarwono pun membawa Pingkan sebagai guide-nya selama di Manado. Pingkan bertemu keluarga besar almarhum ayahnya yang Manado. Ia mulai dipojokkan oleh pertanyaan tentang hubungannya dengan Sarwono. Apalagi kalau bukan masalah perbedaan yang di mata mereka sangat besar. Bukannya Pingkan (dan Sarwono) tidak menyadarinya. Mereka sudah terlanjur nyaman menetap bertahun-tahun di dalam ruangan kedap suara bernama kasih sayang...
        </p>
        <br>
        <p>
        Apakah ini akan jadi perjalanan perpisahan mereka?
        </p>
        br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Thor Ragnarok</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/ue80QwXMRHg">
      </iframe>
      <br>
        <p>Hidup Lala jungkir balik. Bukan karena loncatan indahnya dari menara sepuluh meter, bukan pula karena ayahnya yang melatihnya dengan keras, tapi karena cinta pertamanya. Yudhis, murid baru di sekolahnya, berhasil menjebak hati Lala hanya untuknya. </p>
        <br>
        <p>Di tahun terakhir SMA-nya, Lala ditarik keluar dari rutinitas lamanya. Hidupnya tak lagi melulu melihat birunya air kolam renang atau kusamnya dinding sekolah. Lala percaya cinta telah membebaskannya, sebab Yudhis selalu sigap menghadirkan pelangi asal Lala berjanji selamanya bersama. </p>
        <br>
        <p>Namun perlahan Lala dan Yudhis harus menghadapi bahwa kasih mereka bisa hadirkan kegelapan. Cinta Yudhis yang awalnya tampak sederhana dan melindungi ternyata rumit dan berbahaya. Janji mereka untuk setia selamanya malah jadi jebakan.</p>
        <br>
        <p>Lala kini mengambang dalam pertanyaan: apa artinya cinta? Apakah seperti loncat indah, yang bila gagal, harus ia terus coba lagi atas nama kesetiaan? Ataukah ia hanya sedang tenggelam dalam kesia-siaan?</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">POSESIF</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/5XyMy7Z5SO4">
      </iframe>
      <br>
        <p>Setelah mendengar muncul berbagai permasalahan di tempat asalnya, Thor (Chris Hemsworth) pulang ke Asgard. Ia kehilangan senjata andalannya dan terperangkap dalam sebuah penjara. </p>
        <br>
        <p>Namun saat itu juga ia harus berlomba dengan waktu untuk menghentikan ‘Ragnarok’ dengan melawan Hela, musuh kuat yang ingin menghancurkan Asgard.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">One Fine Day</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/s377iJorKbY">
      </iframe>
      <br>
        <p>Pertemuan tidak terduga antara MAHESA ( Jefri Nichol ) dan ALANA ( Michelle Ziudith ) di Barcelona membuat keduanya menjadi akrab. Namun keakraban ini disalah gunakan oleh Mahesa. Ternyata cara pandang Alana dan Mahesa jauh berbeda. Akankah pertemuan itu menjadi sebuah cerita cinta antara ALANA dan MAHESA..?</p>
        <br>
        <p>DANU ( Maxime Bouttier ) adalah kekasih ALANA di Barcelona. Danu selalu memberikan perhatian yg sangat istimewa kepada Alana, melalui barang-barang mewah. Danu berfikir itu cukup untuk ALANA. Suatu ketika Danu bertemu dengan Mahesa juga Alana. Danu mengingatkan Alana, untuk berhati-hati dengan MAHESA karena dia seorang penipu. Apakah ucapan DANU terbukti benar..?</p>
        <br>
        <p>Saksikan cerita seutuhnya di ONE FINE DAY</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar5" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Negeri Dongeng</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/NqBv33Ipps0">
      </iframe>
      <br>
        <p>Sebuah film mengenai perjalanan melihat Indonesia, melihat sahabat dan rekan-rekan seperjalanan, juga melihat diri sendiri. NEGERI DONGENG berkisah mengenai 7 sineas muda Indonesia yang mendaki 7 puncak gunung tertinggi di Nusantara berbekal 7 buah kamera, bersama-sama. "Dan mencintai tanah air Indonesia dapat ditumbuhkan dengan mengenal Indonesia bersama rakyatnya dari dekat. Pertumbuhan jiwa yang sehat dari pemuda harus berarti pula pertumbuhan fisik yang sehat. Karena itulah, kami naik gunung." -Soe Hok Gie- </p>
        <br>
        <p>Perjalanan panjang membuat mereka mengupas cerita pada setiap tempat yang disinggahi. Beragam emosi berkecamuk dalam perjalanan. Pertemuan dengan orang-orang baru selama penjelajahan darat, laut, dan udara. Setiap potongan kisahnya akan memperlihatkan betapa Indonesia begitu kaya dan luas untuk dijelajahi bersama-sama. Dan di ujung perjalanan itu, kita akan menemukan arti sebuah perjalanan.</p>
        <br>
        <p>Saksikan cerita seutuhnya di NEGERI DONGENG.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar6" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Mau Jadi Apa?</h4>
      </div>
      <div class="modal-body">
      <br>
       <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/HqD7z8DlBfQ">
      </iframe>
      <br>
        <p>1997. SOLEH SOLIHUN berhasil masuk Universitas Padjadjaran. Namun kondisi kampus yang gersang dan lokasinya yang jauh dari mana-mana, membuat Soleh jadi meragukan pilihannya. "Ngapain sih gue di sini? Mau jadi apa nantinya?" bergolak dalam hati Soleh. Bersama teman-temannya yang sama galaunya—LUKMAN (BORIS BOKIR) si jago gambar; MARSYEL (ADJISDOAIBU), si hopeless romantic yang selalu sial; EKO (AWWE), si penggila musik yang doyan nyela; FEY (ANGGIKA BOLSTERLI), si blasteran yang cantik namun sedikit gila; serta si mulut besar SYARIF (RICKY WATTIMENA)—Soleh akhirnya mendirikan sebuah majalah kampus alternatif. Mengangkat hal-hal ringan seperti musik, film, olahraga dan gosip percintaan antar mahasiswa, majalah itu mereka beri nama Karung Goni, kependekan dari 'Kabar, Ungkapan, Gosip dan Opini'. </p>
        <br>
        <p>Mengelola Karung Goni tidaklah mudah. Soleh dan gengnya harus menghadapi cibiran dari anak-anak Fakta Jatinangor, majalah super serius yang lebih dulu mendominasi kampus, yang dipimpin oleh si arogan PANJI (RONAL SURAPRADJA). Bersamaan dengan itu, Soleh juga harus berjuang merebut hati ROS (AURELIE MOEREMANS), Slanker cantik yang telah membuatnya mabuk kepayang sejak ospek. Deretan karakter dan jalinan konflik ini hadir membentuk MAU JADI APA?—sebuah komedi inspiratif yang diangkat dari kisah nyata komika dan mantan jurnalis Soleh Solihun. Inilah bukti bahwa setiap orang punya jalannya sendiri dalam menjawab pertanyaan "mau jadi apa?".</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar7" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Kaili</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/wbssFM2IVb8">
      </iframe>
      <br>
        <p>Kisah ini berawal dari pertemuan pria dan wanita yang tak disengaja di sebuah perguruan tinggi ternama yang ada di kota Palu. Pertemuan sepasang insan manusia yang bernama Fajar dan Senja yang pada akhirnya menumbuhkan benih-benih cinta </p>
        <br>
        <p>Pada pandangan pertama kala itu, mereka tak langsung mengakui perasaan mereka masing- masing dan hanya berujung pada sebuah komunikasi melalui di Handphone. Setelah sama- sama menyelesaikan studi, kedunya memilih berkarir, Senja menjadi tenaga pengajar di sekolah swasta ternama sedangkan Fajar menghabiskan hari-harinya dengan bekerja di bengkel milik ayahnya.</p>
        <br>
        <p>Perjalanan cinta Fajar dan Senja bak bunga-bunga di musim semi yang terus tumbuh dan berkembang. Hingga akhirnya Fajar memberanikan diri mengungkapkan cintanya dan juga disambut hangat oleh Senja. Namun perjalanan cinta mereka sebenarnya terasa kian berat ketika Fajar yang hanya berstatus sosial biasa-biasa saja sedangkan senja adalah keluarga yang berada.</p>
        <br>
        <p>Keyakinan Fajar akan cintanya membuatnya memberanikan diri menemui ayah Senja, namun siapa sangka sang ayah merupakan penghalang dari dua insan yang dilanda asmara tersebut dan adat istiadat kesukuan pun ikut terbawa-bawa sehingga menjadi bumerang bagi hubungan mereka. Fajar merupakan seorang pria suku Kaili harus dilepaskan oleh Senja sebab paham yang berkembang dimasyarakat tentang suku Kaili memiliki sikap buruk dan tidak pantas untuk menjadi pendamping hidup.</p>
        <br>
        <p>Ujian demi ujian silih berganti menghampiri kehidupan Fajar membuatnya harus meninggalkan cintanya dan menjemput harga dirinya sebagai lelaki yang tak ingin dipandang sebelah mata hanya atas dasar kesukuan. Fajar meneguhkan niatnya merantau ke kota lain. Berbagai tantangan hidup telah dilalui bertahun-tahun dalam perantauan hingga akhirnya Fajar menduduki jabatan penting dalam sebuah perusahaan media tempatnya bekerja.</p>
        <br>
        <p>Akankah kisah Fajar dan Senja berakhir bahagia atau terhenti akibat sudut pandang adat istiadat kesukuan yang masih mendarah daging dan tak pernah terlepas dari paradigma masyarakat moderen sekalipun?</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar8" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Susah Sinyal</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/H4VKzSkOow">
      </iframe>
      <br>
        <p>Ellen (Adinia Wirasti), pengacara yang sukses, adalah seorang single mom yang jarang bisa meluangkan waktu bagi anak tunggalnya Kiara (Aurora Ribero), yang akhirnya tumbuh sebagai remaja pemberontak yang lebih banyak melampiaskan emosinya di media sosial. Mereka tinggal bersama Agatha (Niniek L. Karim), ibunda Ellen yang sangat menyayangi Kiara. Ketika Agatha meninggal terkena serangan jantung, Kiara yang sejak kecil sangat dekat dengan Omanya langsung terguncang. Oleh psikolog, Ellen disarankan untuk mengajak Kiara berlibur, menghabiskan quality time untuk mengobati masa-masa di mana Ellen terlalu sibuk bekerja. Mereka pun pergi ke Sumba, menghabiskan saat-saat menyenangkan berdua. Kiara pulang dengan hati riang. </p>
        <br>
        <p>Di Jakarta, Ellen langsung disambut masalah besar di kantor. Proyek besar yang sedang ia tangani bersama Iwan (Ernest Prakasa) terancam berantakan. Akhirnya karna sibuk, Ellen tidak menepati janjinya untuk menonton Kiara tampil di perlombaan talent show antar SMA yang sudah Kiara persiapkan sejak lama. Kiara pun marah dan pergi ke Sumba sendirian, tempat dimana terakhir kali ia bisa merasakan secercah kebahagiaan. Akankah Kiara bisa percaya lagi pada ibunya? Apakah Ellen sanggup merajut kembali keluarga kecil ini? Saksikan SUSAH SINYAL, tayang di bioskop mulai 21 Desember.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



</body>
</html>
<script>
  var acuan=$(".carousel").offset().top;
  var menu = function(){
    var scrol=$(window).scrollTop();
    if (scrol>acuan) {
      $(".navbar-default").addClass("berubah-menu");
    } else {
      $(".navbar-default").removeClass("berubah-menu");
    }
  } 
menu();
$(window).scroll(function(){
  menu();
});

</script>