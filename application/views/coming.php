<br> <br>
<div class="container jarak-atas">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">COMING SOON THIS MONTH</h3>
      </div>
      <div class="panel-body">
          <div class="row">
            <div class="col-md-2">
            <a href="#gambar1" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/justice2.jpg" alt="...">
                <div class="caption">
                <h5 align="center">JUSTICE LEAGUE</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#gambar2" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/daddy.jpg" alt="...">
                <div class="caption">
                <h5 align="center">DADDY'S HOME 2</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#gambar3" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/cococo.jpg" alt="...">
                <div class="caption">
                <h5 align="center">COCO<br></h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#gambar4" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/cowok.jpg" alt="...">
                <div class="caption">
                <h5 align="center">5 COWOK JAGOAN</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#gambar5" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/chrisye.jpg" alt="...">
                <div class="caption">
                <h5 align="center">CHRISYE</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#gambar6" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/valentine.jpg" alt="...">
                <div class="caption">
                <h5 align="center">VALENTINE</h5>
                </div>
             </div>
             </a>
            </div>
             <div class="col-md-2">
            <a href="#gambar7" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/surat.jpg" alt="...">
                <div class="caption">
                <h5 align="center">SURAT CINTA UNTUK STARLA</h5>
                </div>
             </div>
             </a>
            </div>
             <div class="col-md-2">
            <a href="#gambar8" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/murder.jpg" alt="...">
                <div class="caption">
                <h5 align="center">MURDER ON THE ORIENT EXPRESS</h5>
                </div>
             </div>
             </a>
            </div>
        </div>
      </div>  
    </div>
    </div>

        <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title" align="center">COMING SOON NEXT MONTH</h3>
      </div>
      <div class="panel-body">
          <div class="row">
            <div class="col-md-2">
            <a href="#foto1" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/ayat.jpg" alt="...">
                <div class="caption">
                <h5 align="center">AYAT-AYAT CINTA 2</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#foto2" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/knight.jpg" alt="...">
                <div class="caption">
                <h5 align="center">KNIGHT CHRIS</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#foto3" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/satuhari.jpg" alt="...">
                <div class="caption">
                <h5 align="center">SATU HARI NANTI<br></h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#foto4" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/6days.jpg" alt="...">
                <div class="caption">
                <h5 align="center">6 DAYS</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#foto5" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/first.jpg" alt="...">
                <div class="caption">
                <h5 align="center">FIRST KILL</h5>
                </div>
             </div>
             </a>
            </div>
            <div class="col-md-2">
            <a href="#foto6" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/padington.jpg" alt="...">
                <div class="caption">
                <h5 align="center">PADDINGTON 2</h5>
                </div>
             </div>
             </a>
            </div>
             <div class="col-md-2">
            <a href="#foto7" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/marlina.jpg" alt="...">
                <div class="caption">
                <h5 align="center">MARLINA SI PEMBUNUH DALAM EMPAT BABAK </h5>
                </div>
             </div>
             </a>
            </div>
              <div class="col-md-2">
            <a href="#foto8" data-toggle="modal">
             <div class="thumbnail">
                <img src="<?=base_url()?>asset/img/naura.jpg" alt="...">
                <div class="caption">
                <h5 align="center">NAURA & GENK JUARA</h5>
                </div>
             </div>
             </a>
            </div>

        </div>
      </div>  
    </div>
    </div>

</div>
</div>

<div id="gambar1" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">JUSTICE LEAGUE</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/RGl7IjSaKhE">
      </iframe>
      <br>
        <p>Didorong oleh kembalinya rasa percaya terhadap kemanusiaan dan terinspirasi oleh pengorbanan mulia Superman, Batman (Ben Affleck) mengumpulkan bantuan sekutu barunya Wonder Woman (Gal Gadot), Aquaman (Jason Momoa), Cyborg (Ray Fisher) dan The Flash (Ezra Miller) untuk menghadapi musuh yang jauh lebih berbahaya. Mereka bersatu menghadapi ancaman baru yang telah bangkit.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">DADDY'S HOME2</h4>
      </div>
      <div class="modal-body">
      <br>
       <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/yyW_EX7iRW0">
      </iframe>
      <br>
        <p>Dusty (Mark Wahlberg) dan Brad (Will Ferrell) berencana menghabiskan waktu bersama keluarga untuk memberikan kesan natal terbaik bagi anak-anak mereka. Namun hubungan yang baru mereka jalin itu justru kembali harus diuji ketika ayah Dusty bertingkah aneh (Gibson) dan ayah Brad yang penuh perasaan (Lithgow) datang dan mengacaukan liburan itu.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">COCO</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/Ga6RYejo6Hk">
      </iframe>
      <br>
        <p>Miguel (Anthony Gonzalez) bermimpi untuk menjadi seorang musisi profesional seperti idolanya yang telah lama meninggal, Ernesto de la Cruz. Ia lalu menemukan dirinya secara ajaib masuk ke dalam alam kematian. </p>
        <br>
        <p>Miguel berpetualang bersama Hector (Gael García Bernal) dan bertemu dengan nenek moyangnya yang melarang kehadiran musik di tengah keluarga mereka.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">5 COWOK JAGOAN</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/qRr4UEpdKQc">
      </iframe>
      <br>
        <p>Yanto (Ario Bayu), seorang cleaning service sebuah Perusahaan Obat ternama, harus menyelamatkan kekasihnya Dewi (Tika Bravani) yang diculik oleh segerombolan pasukan bersenjata yang sangat profesional. Di saat kasus tidak menjadi berita dan aparat yang berwenang tidak memberikan solusi, ia memutuskan untuk turun tangan langsung demi menyelamatkan sang kekasih.</p>
        <br>
        <p>Ia lalu mengumpulkan keempat sahabat kecilnya yang dulu pernah membuat janji untuk saling membantu jika salah satu ada yg dalam kesulitan. Dedy (Dwi Sasono) seorang hansip kampung, Danu (Arifin Putra) seorang penipu ulung, Lilo (Muhadkly Acho) seorang Gamer dan ahli komputer, serta Reva (Cornelio Sunny) seorang mantan preman; akhirnya setuju untuk membantu Yanto setelah melewati segala janji dan bujuk rayu. </p>
        <br>
        <p>Usaha penyelamatan Dewi yang juga dibantu oleh seorang Cewek Jagoan bernama Debby (Nirina Zubir) pun akhirnya kembali mempererat persahabatan yang pernah hilang, serta membawa mereka ke sebuah petualangan gila yang tidak pernah terpikirkan atau terbayangkan oleh siapa pun sebelumnya. Sebuah dunia baru yang melewati batas ilmu Science dan logika yang tidak sengaja mereka temukan, dimana adrenaline dan tawa terus memompa dan mengocok perut siapa pun yang menyaksikannya. </p>
        <br>
        <p>Proses "From Zero to Hero" dari orang-orang yang kerap dianggap kecil dan dilihat sebelah mata yang lucu dan menyentuh, patut disimak bersama teman-teman, sahabat dan keluarga.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar5" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">CHRISYE</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/yrLS5TKSMUE">
      </iframe>
      <br>
        <p>Menemukan minat dan bakatnya dalam dunia musik tidak membuat Chrisye tenang sepanjang hidupnya. Sejak remaja, dia harus menentang ayahnya yang menginginkan Chrisye menjadi seorang insinyur. Chrisye diam-diam bermusik dan berjuang membuktikan bahwa bermusik memang pilihan hidupnya dengan lagu Lilin-Lilin Kecil, Aku Cinta Dia dan masih banyak lagi.</p>
        <br>
        <p>Pencarian jati diri Chrisye terus berlanjut. Menikah dengan Yanti, menjadi mu'alaf dan berkeluarga membuka cakrawala baru Chrisye dalam memandang hidup tapi juga menambah kegelisahan baru. Meskipun Chrisye sudah menjadi penyanyi hebat, dia merasa khawatir tidak bisa menafkahi keluarganya hanya dengan kemampuan bernyanyi hingga Chrisye sempat memutuskan untuk berhenti menyanyi.</p>
        <br>
        <p>Kegelisahan demi kegelisahan membawa Chrisye ke dalam perjalanan spiritual yang panjang. Pencarian hakiki tentang makna hidup inilah yang mendasari Chrisye dalam menciptakan lagu “Ketika Tangan dan Kaki Berkata” yang liriknya ditulis oleh Taufik Ismail. Saat itulah, Chrisye benar-benar menemukan titik balik dalam kehidupan dan karir musiknya.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar6" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">VALENTINE</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/1LyUtETf9eI">
      </iframe>
      <br>
        <p>Batavia City, kota indah yang tak lagi aman dihuni. Perampokan, kekerasan, dan berbagai kriminalitas semakin hari semakin merajalela. Di tengah kota yang kacau ini, Srimaya, seorang pelayan kafe yang memiliki mimpi menjadi aktris tidak pernah menyangka kalau impiannya akan mengubah jalan hidupnya. Pertemuannya dengan seorang sutradara film dan sahabatnya, Bono dan Wawan, akan membawanya pada petualangan berbahaya penuh aksi mendebarkan dengan nyawa sebagai taruhannya. Mengubahnya dari gadis biasa menjadi pahlawan harapan Batavia City, Valentine. Yang bertugas membasmi kejahatan</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar7" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">SURAT CINTA UNTUK STARLA</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/2xU9j0SOS6A">
      </iframe>
      <br>
        <p>Hema cowok yang terobsesi dengan cintanya pada alam memiliki keunikan membuat surat cinta untuk alam. Dengan dibantu mesin tik tuanya peninggalan sang kakek mantan jurnalis, Hema membuat mural yang menunjukkan betapa cintanya Hema pada alam ini. Alam adalah satu satunya cinta yang Hema kenal.</p>
        <br>
        <p>Hingga suatu saat Hema mengenal Starla. Gadis cantik dan mandiri yang selama 6 jam telah berhasil membuatnya jatuh cinta. Untuk pertama kalinya Hema menyadari ada surat cinta yang lebih indah daripada surat cinta untuk alam yaitu surat cinta untuk Starla.</p>
        <br>
        <p>Namun di saat Hema dan Starla semakin dekat, tiba tiba sikap Starla berubah. Starla marah dan menjauhi Hema bahkan menyuruh Hema melupakan 6 jam kisah mereka yang menjadi viral di sosial media bahkan disiarkan di radio oleh Athena, sahabat Hema.</p>
        <br>
        <p>Hingga suatu ketika Hema baru mengetahui penyebab perubahan sikap Starla padanya yang berhubungan dengan sebuah rahasia masa lalu keluarga Hema.</p>
        <br>
        <p>Bagaimana kelanjutan hubungan Hema dan Starla?</p>
        <br>
        <p>Bagaimana Kelanjutan Starla dengan Tunangannya Bimo?</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="gambar8" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">MURDER ON THE ORIENT EXPRESS</h4>
      </div>
      <div class="modal-body">
      <br>
      <iframe class="img-responsive" style="width:600px; height: 300px;margin-left: 5px" src="https://www.youtube.com/embed/Mq4m3yAoW8E">
      </iframe>
      <br>
        <p>Sebuah perjalanan kereta mewah melintasi benua Eropa akhirnya menjadi kisah misteri yang paling menegangkan dan mendebarkan yang pernah diceritakan. Berdasarkan novel laris karya Agatha Christie, “Murder on the Orient Express” menceritakan kisah 13 orang penumpang dalam kereta yang menjadi tersangka pembunuhan. </p>
        <br>
        <p>Seorang detektif harus berlomba dengan waktu untuk memecahkan kasus ini sebelum terjadi pembunuhan berikutnya. Kenneth Branagh menjadi sutradara dan memimpin para pemeran bintang seperti Penelope Cruz, Willem Dafoe, Judi Dench, Johnny Depp, Michelle Pfeiffer, Daisy Ridley dan Josh Gad.</p>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Pesan Tiket</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="foto1" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">AYAT-AYAT CINTA 2</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/ayat.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="foto2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">KNIGHT CHRIS</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/knight.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="foto3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">SATU HARI NANTI</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/satuhari.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="foto4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">6 DAYS</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/6days.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div id="foto5" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">FIRST KILL</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/first.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="foto6" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">PADDINGTON 2</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/padington.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="foto7" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">MARLINA SI PEMBUNUH DALAM EMPAT BABAK</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/marlina.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="foto7" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">NAURA & GENK JUARA</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
      <img src ="img/naura.jpg">
      </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Lihat Trailer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>
<script>
  var acuan=$(".carousel").offset().top;
  var menu = function(){
    var scrol=$(window).scrollTop();
    if (scrol>acuan) {
      $(".navbar-default").addClass("berubah-menu");
    } else {
      $(".navbar-default").removeClass("berubah-menu");
    }
  } 
menu();
$(window).scroll(function(){
  menu();
});

</script>