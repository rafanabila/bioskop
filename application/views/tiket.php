<br> <br>

<div class="container jarak-atas">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        <h3 class="panel-title">NOW PLAYING</h3>
      </div>
      <div class="panel-body">
          <div class="row">
            <div class="col-md-3">
             <div class="thumbnail">
                <a href="#tiket1" data-toggle="modal">
                  <img src="<?=base_url()?>asset/img/hujansah.jpg" alt="...">
                </a>
                <div class="caption">
                <h5 align="center">Hujan Bulan Juni</h5>
                </div>
             </div>
             
            </div>
            <div class="col-md-3">
             <div class="thumbnail">
                <a href="#tiket2" data-toggle="modal">
                  <img src="<?=base_url()?>asset/img/thor.jpg" alt="...">
                </a>
                <div class="caption">
                <h5 align="center">Thor Ragnarok</h5>
                </div>
             </div>
            </div>
            <div class="col-md-3">
             <div class="thumbnail">
              <a href="#tiket3" data-toggle="modal">
                <img src="<?=base_url()?>asset/img/posesif.jpg" alt="...">
              </a>
                <div class="caption">
                <h5 align="center">Posesif</h5>
                </div>
             </div>
            </div>
            <div class="col-md-3">
             <div class="thumbnail">
              <a href="#tiket4" data-toggle="modal">
                <img src="<?=base_url()?>asset/img/negeri.jpg" alt="...">
              </a>
                <div class="caption">
                <h5 align="center">Negeri Dongeng</h5>
                </div>
             </div>
            </div>
             <div class="col-md-3">
             <div class="thumbnail">
              <a href="#tiket5" data-toggle="modal">
                <img src="<?=base_url()?>asset/img/one.jpg" alt="...">
              </a>
                <div class="caption">
                <h5 align="center">One Fine Day</h5>
                </div>
             </div>
            </div>
            <div class="col-md-3">
             <div class="thumbnail">
              <a href="#tiket6" data-toggle="modal">
                <img src="<?=base_url()?>asset/img/mau.jpg" alt="...">
              </a>
                <div class="caption">
                <h5 align="center">Mau Jadi Apa?</h5>
                </div>
             </div>
            </div>
             <div class="col-md-3">
             <div class="thumbnail">
              <a href="#tiket7" data-toggle="modal">
                <img src="<?=base_url()?>asset/img/kaili.jpg" alt="...">
              </a>
                <div class="caption">
                <h5 align="center">Kaili</h5>
                </div>
             </div>
            </div>
             <div class="col-md-3">
             <div class="thumbnail">
              <a href="#tiket8" data-toggle="modal">
                <img src="<?=base_url()?>asset/img/susah.jpg" alt="...">
              </a>
                <div class="caption">
                <h5 align="center">Susah Sinyal</h5>
                </div>
             </div>
            </div>
        </div>
      </div>  
    </div>
    </div>
</div>
</div>

<div id="tiket1" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Hujan Bulan Juni</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="tiket2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Thor Ragnarok</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="tiket3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Posesif</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="tiket4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Negeri Dongeng</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="tiket5" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">One Fine Day</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="tiket6" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Mau Jadi Apa?</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="tiket7" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Kaili</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="tiket8" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" align="center">Susah Sinyal</h4>
      </div>
      <div class="modal-body">
      <br>
      <p align="center">
        <!-- Nama -->
      <div class="input-group">
        <span class="input-group-addon">Nama</span>
         <input type="text" class="form-control" placeholder="Nama">
      </div>
      <br>
      <!-- Email -->
      <div class="input-group">
        <span class="input-group-addon">Email</span>
         <input type="text" class="form-control" placeholder="Email">
      </div>
      <!-- Jam -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pukul</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>12.30</option>
          <option>13.45</option>
          <option>17.00</option>
          <option>18.15</option>
          </select>
        </div>
      </div>
      </div>
      <!--Studio -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Studio</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Studio 1</option>
          <option>Studio 2</option>
          <option>Studio 3</option>
          <option>Studio 4</option>

          </select>
        </div>
      </div>
      </div>
        <!--Jumlah tiket -->
      <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Jumlah tiket</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          </select>
        </div>
      </div>
      </div>
      <!--Kursi-->
      <p align="center">
      <div class="checkbox">
        <h4 align="center">Kursi</h4>
<table align="center">
  <tr>
      <td><label><input type="checkbox" value="">A1</label></td>
      <td><label><input type="checkbox" value="">A2</label></td>
      <td><label><input type="checkbox" value="">A3</label></td>
      <td><label><input type="checkbox" value="">A4</label></td>
      <td><label><input type="checkbox" value="">A5</label></td>
      <td><label><input type="checkbox" value="">A6</label></td>
      <td> [ ] </td>
     <td><label><input type="checkbox" value="">A7</label></td>
      <td><label><input type="checkbox" value="">A8</label></td>
      <td><label><input type="checkbox" value="">A9</label></td>
      <td><label><input type="checkbox" value="">A10</label></td>
      <td><label><input type="checkbox" value="">A11</label></td>
      <td><label><input type="checkbox" value="">A12</label></td>
  </tr>
</div> 
      <div class="checkbox">
<tr>
      <td><label><input type="checkbox" value="">B1</label></td>
      <td><label><input type="checkbox" value="">B2</label></td>
      <td><label><input type="checkbox" value="">B3</label></td>
      <td><label><input type="checkbox" value="">B4</label></td>
      <td><label><input type="checkbox" value="">B5</label></td>
      <td><label><input type="checkbox" value="">B6</label></td>
      <td> [ ] </td>
      <td><label><input type="checkbox" value="">B7</label></td>
      <td><label><input type="checkbox" value="">B8</label></td>
      <td><label><input type="checkbox" value="">B9</label></td>
      <td><label><input type="checkbox" value="">B10</label></td>
      <td><label><input type="checkbox" value="">B11</label></td>
     <td><label><input type="checkbox" value="">B12</label></td>
</tr>
</div>
     <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">C1</label></td>
  <td><label><input type="checkbox" value="">C2</label></td>
  <td><label><input type="checkbox" value="">C3</label></td>
  <td><label><input type="checkbox" value="">C4</label></td>
  <td><label><input type="checkbox" value="">C5</label></td>
  <td><label><input type="checkbox" value="">C6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">C7</label></td>
  <td><label><input type="checkbox" value="">C8</label></td>
  <td><label><input type="checkbox" value="">C9</label></td>
  <td><label><input type="checkbox" value="">C10</label></td>
  <td><label><input type="checkbox" value="">C11</label></td>
  <td><label><input type="checkbox" value="">C12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">D1</label></td>
  <td><label><input type="checkbox" value="">D2</label></td>
  <td><label><input type="checkbox" value="">D3</label></td>
  <td><label><input type="checkbox" value="">D4</label></td>
  <td><label><input type="checkbox" value="">D5</label></td>
  <td><label><input type="checkbox" value="">D6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">D7</label></td>
  <td><label><input type="checkbox" value="">D8</label></td>
  <td><label><input type="checkbox" value="">D9</label></td>
  <td><label><input type="checkbox" value="">D10</label></td>
  <td><label><input type="checkbox" value="">D11</label></td>
  <td><label><input type="checkbox" value="">D12</label></td>
</tr>
</div>
       <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">E1</label></td>
  <td><label><input type="checkbox" value="">E2</label></td>
  <td><label><input type="checkbox" value="">E3</label></td>
  <td><label><input type="checkbox" value="">E4</label></td>
  <td><label><input type="checkbox" value="">E5</label></td>
  <td><label><input type="checkbox" value="">E6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">E7</label></td>
  <td><label><input type="checkbox" value="">E8</label></td>
  <td><label><input type="checkbox" value="">E9</label></td>
  <td><label><input type="checkbox" value="">E10</label></td>
  <td><label><input type="checkbox" value="">E11</label></td>
  <td><label><input type="checkbox" value="">E12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">F1</label></td>
  <td><label><input type="checkbox" value="">F2</label></td>
  <td><label><input type="checkbox" value="">F3</label></td>
  <td><label><input type="checkbox" value="">F4</label></td>
  <td><label><input type="checkbox" value="">F5</label></td>
  <td><label><input type="checkbox" value="">F6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">F7</label></td>
  <td><label><input type="checkbox" value="">F8</label></td>
  <td><label><input type="checkbox" value="">F9</label></td>
  <td><label><input type="checkbox" value="">F10</label></td>
  <td><label><input type="checkbox" value="">F11</label></td>
  <td><label><input type="checkbox" value="">F12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">G1</label></td>
  <td><label><input type="checkbox" value="">G2</label></td>
  <td><label><input type="checkbox" value="">G3</label></td>
  <td><label><input type="checkbox" value="">G4</label></td>
  <td><label><input type="checkbox" value="">G5</label></td>
  <td><label><input type="checkbox" value="">G6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">G7</label></td>
  <td><label><input type="checkbox" value="">G8</label></td>
  <td><label><input type="checkbox" value="">G9</label></td>
  <td><label><input type="checkbox" value="">G10</label></td>
  <td><label><input type="checkbox" value="">G11</label></td>
  <td><label><input type="checkbox" value="">G12</label></td>
</tr>
</div>
    <div class="checkbox">
<tr>
  <td><label><input type="checkbox" value="">H1</label></td>
  <td><label><input type="checkbox" value="">H2</label></td>
  <td><label><input type="checkbox" value="">H3</label></td>
  <td><label><input type="checkbox" value="">H4</label></td>
  <td><label><input type="checkbox" value="">H5</label></td>
  <td><label><input type="checkbox" value="">H6</label></td>
  <td> [ ] </td>
  <td><label><input type="checkbox" value="">H7</label></td>
  <td><label><input type="checkbox" value="">H8</label></td>
  <td><label><input type="checkbox" value="">H9</label></td>
  <td><label><input type="checkbox" value="">H10</label></td>
  <td><label><input type="checkbox" value="">H11</label></td>
  <td><label><input type="checkbox" value="">H12</label></td>
</tr>
</div>
</table>
</div>
</p>
    <!--Pembayaran-->
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="form-group">
          <label for="sel1">Pembayaran</label>
          <select class="form-control" id="sel1">
          <option></option>
          <option>Cash</option>
          <option>Kredit</option>
          <option>Debit</option>
          </select>
        </div>
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Book Now</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



</body>
</html>
<script>
  var acuan=$(".carousel").offset().top;
  var menu = function(){
    var scrol=$(window).scrollTop();
    if (scrol>acuan) {
      $(".navbar-default").addClass("berubah-menu");
    } else {
      $(".navbar-default").removeClass("berubah-menu");
    }
  } 
menu();
$(window).scroll(function(){
  menu();
});

</script>