<!DOCTYPE html>
<html>
<head>
  <title>Welcome</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/style-login.css">
  <link rel="icon" href="<?=base_url()?>asset/img/aaa.png">
  <script type="text/javascript" src="js/jquery-3.2.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
<div class="col-md-4 col-md-offset-4 jarak-atas">
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title text-center">REGISTER</h3>
  </div>
  <div class="panel-body">
    <?php
  if($this->session->flashdata('pesan')!=null){
    echo "<div class='alert alert-danger'>".$this->session->flashdata('pesan')."</div>";
  }
?>
  <form action="<?=base_url('index.php/bioskop/simpan')?>" method="post">
      <div class="input-group">
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-user"></span> 
        </span>
        <input name="nama" type="text" class="form-control" placeholder="Full name">
      </div>
      <br>
      <div class="input-group">
        <span class="input-group-addon">
          @
        </span>
        <input name="email" type="email" class="form-control" placeholder="Email">
      </div>
      <br>
      <div class="input-group">
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-phone"></span> 
        </span>
        <input name="username" type="text" class="form-control" placeholder="Username">
      </div>
      <br>
      <div class="input-group">
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-lock"></span> 
        </span>
        <input name="password" type="password" class="form-control" placeholder="Password">
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <br>
        <div class="col-xs-4">
          <input type="submit" class="btn btn-primary btn-block btn-flat" value="Register" name="submit">
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="<?=base_url()?>index.php/bioskop/login" class="text-center">I already have a membership</a>
  </div>
                        </div>
</div>
</div>
</body>
</html>
