<!DOCTYPE html>
<html>
<head>
  <title>Cinema 21</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/style.css">
  <link rel="icon" href="<?=base_url()?>asset/img/aaa.png">
  <script type="text/javascript" src="<?=base_url()?>asset/js/jquery-3.2.js"></script>
  <script type="text/javascript" src="<?=base_url()?>asset/js/bootstrap.js"></script>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    <a class="navbar-brand" href="<?=base_url()?>index.php/"><img src="<?=base_url()?>asset/img/logo.png"></a>
   </div>
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?=base_url()?>index.php/"><span class="glyphicon glyphicon-home"></span> HOME</a></li>
        <li><a href="<?=base_url()?>index.php/bioskop/coming"><span class="glyphicon glyphicon-film"></span> COMING SOON</a></li>
        <li><a href="<?=base_url()?>index.php/bioskop/ks"><span class="glyphicon glyphicon-inbox"></span> CRITICISM & SUGGESTION</a></li>
        <!--<li><a href="<?=base_url()?>index.php/bioskop/tiket"><span class="glyphicon glyphicon-saved"></span> TICKET BOOKING</a></li>-->
        <li><a href="<?=base_url()?>index.php/bioskop/tm_film"><span class="glyphicon glyphicon-saved"></span> TICKET BOOKING</a></li>
        <li><a href="<?=base_url()?>index.php/pesanan"><span class="glyphicon glyphicon-list-alt"></span> HISTORY</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <!--<li><a href="<?=base_url()?>index.php/bioskop/register"><span class="glyphicon glyphicon-registration-mark"></span> DAFTAR</a></li>-->
      <li><a href="<?=base_url('index.php/cart')?>">
              <span class="glyphicon glyphicon-shopping-cart"></span> 
              <span class="label label-success">
                <?= $this->cart->total_items();?>
              </span>
            </a></li>
        <li><a href="#"><span class="glyphicon glyphicon-user"></span>
          <?php echo $this->session->userdata('username');?></a></li>
        <?php if($this->session->userdata('login')==TRUE){?>
        <li><a href="<?=base_url()?>index.php/bioskop/logout"><span class="glyphicon glyphicon-log-out"></span> LOGOUT</a></li><?php } else{?>
        <li><a href="<?=base_url()?>index.php/bioskop/login"><span class="glyphicon glyphicon-log-out"></span> LOGIN</a></li><?php }?>
    </ul>
    </div>
  </div>
</nav>
<?php $this->load->view($konten); ?>

<!--
<div class="container jarak-atas">
  <div class="row">
    <div class="col-md-25">
      <div class="panel panel-default">
      <div class="panel-body">
          <div class="jumbotron">
    <h4>Menu</h4> <hr><br><br><br>
    <p><a href="<?= base_url()?>asset/">Home </a><br>
        <a href="<?= base_url()?>asset/">About </a><br>
        <a href="<?= base_url()?>asset/">Terms of Use </a><br>
        <a href="<?= base_url()?>asset/">Credit </a><br>
        <a href="<?= base_url()?>asset/"> Profile</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
-->