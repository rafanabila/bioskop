<br><br><br>
<div class="col-md-6 jarak-atas col-md-offset-3">
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">Upload Data Bukti Pembayaran</h2>
	</div>
	<div class="panel-body" style="background-color: white">
<?php if ($this->session->flashdata('pesan')): ?>
	<div class="alert alert-success">
		<?= $this->session->flashdata('pesan'); ?>
	</div>
	
<?php endif ?>    

<form action="<?=base_url('index.php/cart/proses_upload')?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id_nota" value="<?= $id_nota ?>">
	<input type="file" name="bukti" class="form-control"><br>
	<input type="submit" name="upload" value="UPLOAD" class="btn btn-success">
</form>
</div></div></div>