<!DOCTYPE html>
<html>
<head>
  <title>Welcome</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/style-login.css">
  <link rel="icon" href="<?=base_url()?>asset/img/aaa.png">
  <script type="text/javascript" src="js/jquery-3.2.js"></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body>
<div class="col-md-4 col-md-offset-4 jarak-atas">
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title text-center">LOGIN</h3>
  </div>
  <div class="panel-body">
    <?php
  if($this->session->flashdata('pesan')!=null){
    echo "<div class='alert alert-success'>".$this->session->flashdata('pesan')."</div>";
  }
?>
  <form action="<?=base_url('index.php/bioskop/simpan')?>" method="post">
      <div class="form-group has-feedback">
          <div class="form-group has-feedback">
        <input name="username" type="text" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <input type="submit" name="login" class="btn btn-primary btn-block btn-flat" value="Sign In">
        </div>
    </form>
    <a href="<?=base_url()?>index.php/admin/register" class="text-center">Register a new membership</a>
  </div>
                        </div>
</div>
</div>
</body>
</html>
